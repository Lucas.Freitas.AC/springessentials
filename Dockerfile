FROM openjdk:11
WORKDIR /app
COPY target/springEssentials-0.0.1-SNAPSHOT.jar /app/springEssentials.jar
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=docker","springEssentials.jar"]