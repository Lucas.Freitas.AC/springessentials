package com.company.springEssentials.model.dto;

import com.company.springEssentials.model.dto.input.RestaurantInputModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RestaurantModel extends RestaurantInputModel {

    private Long id;

    public RestaurantModel(Long id, @NotBlank String name, @NotNull @PositiveOrZero BigDecimal freightRate, @NotBlank String cookery) {
        super(name, freightRate, cookery);
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        RestaurantModel that = (RestaurantModel) o;

        return new EqualsBuilder().appendSuper(super.equals(o)).append(id, that.id).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).appendSuper(super.hashCode()).append(id).toHashCode();
    }
}
