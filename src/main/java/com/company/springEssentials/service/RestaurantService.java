package com.company.springEssentials.service;

import com.company.springEssentials.exception.RestaurantNotFoundException;
import com.company.springEssentials.model.Restaurant;
import com.company.springEssentials.repository.RestaurantRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RestaurantService {

    @Autowired
    private RestaurantRepository restaurantRepository;

    public Restaurant save(Restaurant restaurant) {
        return restaurantRepository.save(restaurant);
    }

    public void delete(Long restaurantId) {
        fetchOrFail(restaurantId);

        restaurantRepository.deleteById(restaurantId);
    }

    public List<Restaurant> findAll() {
        return restaurantRepository.findAll();
    }

    public Restaurant update(Restaurant restaurant, Long restaurantId) {
        Restaurant currentRestaurant = fetchOrFail(restaurantId);

        BeanUtils.copyProperties(restaurant, currentRestaurant, "id", "registrationDate");

        return save(currentRestaurant);
    }

    public Restaurant fetchOrFail(Long restaurantId) {
        return restaurantRepository.findById(restaurantId)
                .orElseThrow(() -> new RestaurantNotFoundException(restaurantId));
    }

}
