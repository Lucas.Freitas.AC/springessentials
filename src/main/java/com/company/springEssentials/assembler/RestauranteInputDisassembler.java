package com.company.springEssentials.assembler;

import com.company.springEssentials.model.Restaurant;
import com.company.springEssentials.model.dto.input.RestaurantInputModel;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RestauranteInputDisassembler {

    @Autowired
    private ModelMapper modelMapper;

    public Restaurant toDomainObject(RestaurantInputModel restaurantInputModel) {
        return modelMapper.map(restaurantInputModel, Restaurant.class);
    }

}
