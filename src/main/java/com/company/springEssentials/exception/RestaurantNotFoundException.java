package com.company.springEssentials.exception;

public class RestaurantNotFoundException extends RuntimeException {

    public RestaurantNotFoundException(String mensagem) {
        super(mensagem);
    }

    public RestaurantNotFoundException(Long restaurantId) {
        this(String.format("There is no restaurant registration with code %d", restaurantId));
    }

}
