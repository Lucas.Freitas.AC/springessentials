package com.company.springEssentials.controller;

import com.company.springEssentials.assembler.RestaurantModelAssembler;
import com.company.springEssentials.assembler.RestauranteInputDisassembler;
import com.company.springEssentials.model.Restaurant;
import com.company.springEssentials.model.dto.RestaurantModel;
import com.company.springEssentials.model.dto.input.RestaurantInputModel;
import com.company.springEssentials.service.RestaurantService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/restaurants")
public class RestaurantController {

    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private RestaurantModelAssembler restaurantModelAssembler;

    @Autowired
    private RestauranteInputDisassembler restauranteInputDisassembler;

    @GetMapping
    public List<RestaurantModel> findAll() {
        log.info("Listing all registered restaurants");

        return restaurantModelAssembler.toCollectionModel(restaurantService.findAll());
    }

    @GetMapping("/{restaurantId}")
    public RestaurantModel findById(@PathVariable Long restaurantId) {
        log.info("Find a restaurant by id {}", restaurantId);

        return restaurantModelAssembler.toModel(restaurantService.fetchOrFail(restaurantId));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public RestaurantModel save(@RequestBody @Valid RestaurantInputModel restaurantInputModel) {
        log.info("Registering a new restaurant {} ", restaurantInputModel.toString());

        Restaurant restaurant = restauranteInputDisassembler.toDomainObject(restaurantInputModel);

        return restaurantModelAssembler.toModel(restaurantService.save(restaurant));
    }

    @PutMapping("/{restaurantId}")
    public RestaurantModel update(@PathVariable Long restaurantId, @RequestBody @Valid RestaurantInputModel restaurantInputModel) {
        log.info("Updating the restaurant with id {} ", restaurantId);

        Restaurant restaurant = restauranteInputDisassembler.toDomainObject(restaurantInputModel);

        return restaurantModelAssembler.toModel(restaurantService.update(restaurant, restaurantId));
    }

    @DeleteMapping("/{restaurantId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long restaurantId) {
        log.info("Deleting the restaurant with id {} ", restaurantId);

        restaurantService.delete(restaurantId);
    }

}
