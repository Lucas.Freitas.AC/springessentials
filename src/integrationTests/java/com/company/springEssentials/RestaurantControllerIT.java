package com.company.springEssentials;


import com.company.springEssentials.model.Restaurant;
import com.company.springEssentials.repository.RestaurantRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestaurantControllerIT {

    @LocalServerPort
    private int port;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RestaurantRepository restaurantRepository;

    @BeforeEach
    public void setUp() {
        data();
    }

    @Test
    public void shouldGet2RestaurantsWhenConsultingRestaurants() {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();

        RestAssured.given()
                .basePath("/restaurants")
                .port(port)
                .auth().basic("lucas", "senha")
                .accept(ContentType.JSON)
                .when()
                .get()
                .then()
                .body("", Matchers.hasSize(2));
    }

    @Test
    public void shouldReturnStatus201WhenCreateRestaurant() throws Exception {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();

        RestAssured.given()
                .basePath("/restaurants")
                .port(port)
                .auth().basic("lucas", "senha")
                .body(objectMapper.writeValueAsString(new Restaurant("Lucas", BigDecimal.valueOf(5.5), "Italiana")))
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .post()
                .then()
                .statusCode(HttpStatus.CREATED.value());
    }

    @Test
    public void shouldReturnStatus200WhenUpdateRestaurant() throws Exception {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();

        RestAssured.given()
                .basePath("/restaurants")
                .pathParam("restaurantId", 1)
                .port(port)
                .auth().basic("lucas", "senha")
                .body(objectMapper.writeValueAsString(new Restaurant("Alex", BigDecimal.valueOf(1.5), "Portuguesa")))
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .when()
                .put("/{restaurantId}")
                .then()
                .statusCode(HttpStatus.OK.value())
                .body("name", Matchers.equalTo("Alex"));
    }

    @Test
    public void shouldReturnStatus204WhenDeleteRestaurant() throws Exception {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();

        RestAssured.given()
                .basePath("/restaurants")
                .pathParam("restaurantId", 1)
                .port(port)
                .auth().basic("lucas", "senha")
                .accept(ContentType.JSON)
                .when()
                .delete("/{restaurantId}")
                .then()
                .statusCode(HttpStatus.NO_CONTENT.value());
    }

    private void data() {

        Restaurant restaurant = new Restaurant();
        restaurant.setName("Lucas");
        restaurant.setCookery("Brasileira");
        restaurant.setFreightRate(BigDecimal.valueOf(5.5));
        restaurantRepository.save(restaurant);

        Restaurant otherRestaurant = new Restaurant();
        otherRestaurant.setName("Danilo");
        otherRestaurant.setCookery("Italiana");
        otherRestaurant.setFreightRate(BigDecimal.valueOf(6.5));
        restaurantRepository.save(otherRestaurant);

    }


}
