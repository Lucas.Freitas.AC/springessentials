package com.company.springEssentials;

import com.company.springEssentials.assembler.RestaurantModelAssembler;
import com.company.springEssentials.assembler.RestauranteInputDisassembler;
import com.company.springEssentials.controller.RestaurantController;
import com.company.springEssentials.model.Restaurant;
import com.company.springEssentials.model.dto.RestaurantModel;
import com.company.springEssentials.model.dto.input.RestaurantInputModel;
import com.company.springEssentials.service.RestaurantService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@WebMvcTest(RestaurantController.class)
@AutoConfigureMockMvc
public class RestaurantControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private RestaurantService restaurantService;

    @Autowired
    private RestaurantModelAssembler restaurantModelAssembler;

    @TestConfiguration
    static class TestConfig {

        @Bean
        public RestauranteInputDisassembler restauranteInputDisassembler() {
            return new RestauranteInputDisassembler();
        }

        @Bean
        public RestaurantModelAssembler restaurantModelAssembler() {
            return new RestaurantModelAssembler();
        }

        @Bean
        public ModelMapper modelMapper() {
            return new ModelMapper();
        }

    }

    @Test
    public void shouldGetListRestaurant() throws Exception {
        Mockito.when(restaurantService.findAll()).thenReturn(restaurants());

        var mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/restaurants")
                .header("Authorization", "Basic bHVjYXM6c2VuaGE="))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        String jsonResponse = mvcResult.getResponse().getContentAsString();

        TypeReference<List<RestaurantModel>> typeRef
                = new TypeReference<>() {
        };
        List<RestaurantModel> restaurantModel = objectMapper.readValue(jsonResponse, typeRef);

        Assertions.assertEquals(restaurantModel, restaurantsModel());

    }

    @Test
    public void shouldGetOneRestaurant() throws Exception {
        Mockito.when(restaurantService.fetchOrFail(1L)).thenReturn(returnCorrectRestaurant());

        var mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/restaurants/{restaurantId}", 1L)
                .header("Authorization", "Basic bHVjYXM6c2VuaGE="))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        String jsonResponse = mvcResult.getResponse().getContentAsString();

        var restaurantModel = objectMapper.readValue(jsonResponse, RestaurantModel.class);

        Assertions.assertEquals(restaurantModel, returnCorrectRestaurantModel());

    }

    @Test
    public void shouldCreatePerson() throws Exception {
        Mockito.when(restaurantService.save(createCorrectRestaurant())).thenReturn(returnCorrectRestaurant());

        var mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/restaurants")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Basic bHVjYXM6c2VuaGE=")
                .content(objectMapper.writeValueAsString(createCorrectInputRestaurant())))
                .andExpect(MockMvcResultMatchers.status().isCreated()).andReturn();

        String jsonResponse = mvcResult.getResponse().getContentAsString();

        var restaurantModel = objectMapper.readValue(jsonResponse, RestaurantModel.class);

        Assertions.assertEquals(restaurantModel, returnCorrectRestaurantModel());
    }

    @Test
    public void shouldUpdatePerson() throws Exception {
        Mockito.when(restaurantService.update(createCorrectRestaurant(), 1L)).thenReturn(returnCorrectRestaurant());

        var mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/restaurants/{restaurantId}", 1)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Basic bHVjYXM6c2VuaGE=")
                .content(objectMapper.writeValueAsString(createCorrectInputRestaurant())))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        String jsonResponse = mvcResult.getResponse().getContentAsString();

        var restaurantModel = objectMapper.readValue(jsonResponse, RestaurantModel.class);

        Assertions.assertEquals(restaurantModel, returnCorrectRestaurantModel());
    }

    @Test
    public void shouldRemoveRestaurant() throws Exception {

        Mockito.doNothing().when(restaurantService).delete(1L);

        mockMvc.perform(MockMvcRequestBuilders.delete("/restaurants/{personId}", "1")
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", "Basic bHVjYXM6c2VuaGE="))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        Mockito.verify(restaurantService, Mockito.times(1)).delete(1L);
    }

    @Test
    public void shouldReceiveMessageWhenNameIsNull() {

        Exception exception = Assertions.assertThrows(NullPointerException.class, () -> {
            restaurantService.save(createRestaurantWithNullName());
        });

        Assertions.assertTrue(exception.getMessage().contains("name is marked non-null but is null"));

    }

    @Test
    public void shouldReceiveMessageWhenCookeryIsNull() {

        Exception exception = Assertions.assertThrows(NullPointerException.class, () -> {
            restaurantService.save(createRestaurantWithNullCookery());
        });

        Assertions.assertTrue(exception.getMessage().contains("cookery is marked non-null but is null"));

    }

    private List<Restaurant> restaurants() {
        var restaurantList = List.of(
                new Restaurant(1L, "Lucas", BigDecimal.valueOf(5.5), "Italiana", LocalDateTime.of(2021, 05, 21, 13, 10), LocalDateTime.of(2021, 05, 21, 13, 10)),
                new Restaurant(2L, "Danilo", BigDecimal.valueOf(6.5), "Japonesa", LocalDateTime.of(2021, 05, 21, 14, 10), LocalDateTime.of(2021, 05, 21, 14, 10)),
                new Restaurant(3L, "Rafael", BigDecimal.valueOf(3.5), "Brasileira", LocalDateTime.of(2021, 05, 21, 15, 10), LocalDateTime.of(2021, 05, 21, 15, 10))
        );

        return restaurantList;
    }

    private List<RestaurantModel> restaurantsModel() {

        return restaurantModelAssembler.toCollectionModel(restaurants());

    }

    private Restaurant createCorrectRestaurant() {
        return new Restaurant("Lucas", BigDecimal.valueOf(5.5), "Italiana");
    }

    private RestaurantInputModel createCorrectInputRestaurant() {
        return new RestaurantInputModel("Lucas", BigDecimal.valueOf(5.5), "Italiana");
    }

    private RestaurantModel returnCorrectRestaurantModel() {
        return restaurantModelAssembler.toModel(returnCorrectRestaurant());
    }

    private Restaurant returnCorrectRestaurant() {
        return new Restaurant(1L, "Lucas", BigDecimal.valueOf(5.5), "Italiana", LocalDateTime.of(2021, 05, 21, 13, 10), LocalDateTime.of(2021, 05, 21, 13, 10));
    }


    private Restaurant createRestaurantWithNullName() {
        return new Restaurant(null, BigDecimal.valueOf(5.5), "Italiana");
    }

    private Restaurant createRestaurantWithNullCookery() {
        return new Restaurant("Lucas", BigDecimal.valueOf(5.5), null);
    }

}